@extends('layouts.app')

<style>
	.password-icon {
		float: right;
		position: relative;
		margin: -25px 350px 0 0;
		cursor: pointer;
	}
	#uploadImage{
		opacity: 0;
		position: absolute;
		z-index: -1;
	}
	#labelImage{
		cursor: pointer;
	}
</style>
@section('content')
<div class="row">

	<div class="col-8 col-md-8 pt-md-4">
		@auth('tutor')
			<h2>{{ Auth::guard('tutor')->user()->tutor->nombres }}</h2>
			<h3 class="text-secondary">{{ Auth::guard('tutor')->user()->tutor->titulo }}</h3>
		@endauth
		@auth('participantes')
			<h2>{{ Auth::guard('participantes')->user()->nombres }}</h2>
			<h3 class="text-secondary">{{ Auth::guard('participantes')->user()->codigo }}</h3>
		@endauth
		
		<hr>
	</div>

</div>

<div class="row mt-4">
	<div class="col-12 col-md-12">

		<div class="card shadow-sm mb-5 bg-white rounded">
			<div class="card-header">
				<div class="row">
					<div class="col-9 align-self-center ">
						Configuracion de usuario
					</div>
					<div class="col-3">
						<a href="#" class="btn btn-secondary float-right edit"><i class="fas fa-edit"></i> Editar</a>
						<a href="#" class="btn btn-success float-right save" style="display:none;"><i class="fas fa-save" ></i> Guardar</a>
					
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-9 align-self-center" >
						<div class="form-group">

							<label for="">Cambio de contraseña</label>
							<input type="password" class="form-control w-50" id="password1" name="password1" disabled>
							<span class="fa fa-fw fa-eye password-icon show-password"></span>
							<span class="text-danger">Si deja este campo vacio, se toma en cuenta que no cambiará la contraseña, será la misma</span>

						</div>
					</div>
					<div class="col-3">

						
						<img src="https://www.gravatar.com/avatar?d" class="w-75 rounded-circle">
						<button type="button" id="showModal" class="btn btn-success">Cambiar imagen</button>
					
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

<div class="row">

	<div class="col-12">
		<div class="card">
			<div class="card-header">
				<div class="row">
					<div class="col-9 align-self-center">
						Datos personales
					</div>
				</div>
			</div>
			<div class="col-12 col-md-10">
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">Nombres</label>
							<input class="form-control" id="Nombres" type="text" value="{{ Auth::guard('tutor')->user()->tutor->nombres }}" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">Nombres</label>
							<input class="form-control" id="Nombres" type="text" value="{{ Auth::guard('participantes')->user()->nombres }}" disabled></input>
						@endauth
						</div>
					</div>
					<div class="col-12 col-md-6">
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">Apellidos</label>
							<input class="form-control" id="Apellidos" type="text" value="{{ Auth::guard('tutor')->user()->tutor->apellidos }}" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">Apellidos</label>
							<input class="form-control" id="Apellidos" type="text" value="{{ Auth::guard('participantes')->user()->apellidos }}" disabled></input>
						@endauth
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-4">
					
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">DUI</label>
							<input class="form-control" id="DUI" type="text" value="{{ Auth::guard('tutor')->user()->tutor->dui }}" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">DUI</label>
							<input class="form-control" id="DUI" id="Nombres" type="text" value="{{ Auth::guard('participantes')->user()->dui }}" disabled></input>
						@endauth
						
						</div>
					
					</div>
					<div class="col-12 col-md-4">
					
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">NIT</label>
							<input class="form-control" id="nit" type="text" value="" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">NIT</label>
							<input class="form-control" id="nit" type="text" value="{{ Auth::guard('participantes')->user()->nit }}" disabled></input>
						@endauth
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">Correo electrónico</label>
							<input class="form-control" id="email" type="text" value="{{ Auth::guard('tutor')->user()->tutor->correo }}" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">Correo electrónico</label>
							<input class="form-control" id="email" type="text" value="{{ Auth::guard('participantes')->user()->correo }}" disabled></input>
						@endauth
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-4">
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">Telefono</label>
							<input class="form-control" id="Telefono" type="text" value="{{ Auth::guard('tutor')->user()->tutor->telefono }}" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">Telefono</label>
							<input class="form-control" id="Telefono" type="text" value="{{ Auth::guard('participantes')->user()->telefono }}" disabled></input>
						@endauth
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-4">
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">Genero</label>
							<input class="form-control" id="Genero" name="Genero" type="text" value="{{ Auth::guard('tutor')->user()->tutor->genero }}" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">Genero</label>
							<input class="form-control" id="Genero" name="Genero" type="text" value="{{ Auth::guard('participantes')->user()->genero }}" disabled></input>
						@endauth
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-4">
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">Estado Civil</label>
							<input class="form-control" id="ecivil" name="ecivil" type="text" value="" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">Estado Civil</label>
							<input class="form-control" id="ecivil" name="ecivil" type="text" value="{{ Auth::guard('participantes')->user()->ecivil }}" disabled></input>
						@endauth
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-4">
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">Nacionalidad</label>
							<input class="form-control" id="Nacionalidad" name="Nacionalidad" type="text" value="" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">Nacionalidad</label>
							<input class="form-control" id="Nacionalidad" name="Nacionalidad" type="text" value="{{ Auth::guard('participantes')->user()->nacionalidad }}" disabled></input>
						@endauth
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">Municipio</label>
							<input class="form-control" id="Municipio" name="Municipio" type="text" value="" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">Municipio</label>
							<input class="form-control" id="Municipio" name="Municipio" type="text" value="{{ Auth::guard('participantes')->user()->municipio }}" disabled></input>
						@endauth
						</div>
					</div>
					<div class="col-12 col-md-4">
						<div class="form-group">
						@auth('tutor')
							<label class="control-label mt-1">Departamento</label>
							<input class="form-control" id="Departamento" name="Departmento" type="text" value="" disabled></input>
						@endauth
						@auth('participantes')
							<label class="control-label mt-1">Departamento</label>
							<input class="form-control" id="Departamento" name="Departmento" type="text" value="{{ Auth::guard('participantes')->user()->departamento }}" disabled></input>
						@endauth
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalImagen" tabindex="-1" role="dialog" aria-labelledby="actualiarModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="actualiarModalLabel">Cambiar imagen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group m-2 w-100">
                        <label for="uploadImage" class="btn btn-primary mt-2">Cambiar imagen</label>
						<input type="file" name="uploadImage" id="uploadImage" >
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnCambios">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" charset="utf-8"></script>
<script>
	$(document).ready(function(){
		
		let modalImage = $("#modalImagen")
		let showModalButton = $("#showModal")

		let showPassword = $(".show-password")
		let editButon = $(".edit");
		let saveButton = $(".save")
		

		let elementsDisabled = document.querySelectorAll("input[disabled]")
		let passwordInput = document.querySelector("#password1")
		
		
		let Nombres = document.querySelector("#Nombres");
		let Apellidos = document.querySelector("#Apellidos");
		let DUI = document.querySelector("#DUI");
		let email = document.querySelector("#email")
		let telefono = document.querySelector("#Telefono");
		let genero = document.querySelector("#Genero");

		showPassword.on("click",function(e){
			
			if ( passwordInput.type === "text" ) {
				passwordInput.type = "password"
				showPassword.removeClass('fa-eye-slash');
			} else {
				passwordInput.type = "text"
				showPassword.addClass("fa-eye-slash");
			}
		})

		editButon.on("click",function(e){
			e.preventDefault();
			
			elementsDisabled.forEach(e => {
				//console.log(e)
				e.removeAttribute('disabled')
			})
			editButon.hide();
			saveButton.show()
		})

		saveButton.on("click",function(e){
			e.preventDefault();
			
			let elementsEnabled = document.querySelectorAll("input")
			//Luego de guardar la informacion, se tiene que bloquear de nuevo
			//O no
			elementsEnabled.forEach(e => {
				//console.log(e)
				e.setAttribute('disabled','disabled')
			})

			$.ajax('/cuenta/updatePassword',{
				method:"POST",
				data:{"_token": "{{ csrf_token() }}","password":passwordInput.value},
				success:function(response,status,xhr){
					if(xhr.status === 200)
					{	
						toastr.success('Contraseña actualizada');
					}else{
						if(xhr.status === 204){
							toastr.success('La contraseña se mantendrá');
						}
					}
					
				},
				error:function(response,s,xhr){
					if(xhr.status === 400){
						toastr.error('Error en actualizar contraseña');
					}
				}
			})
			
			$.ajax('/cuenta/updateUsuario',{
				method:"POST",
				data:{"_token": "{{ csrf_token() }}","Nombres":Nombres.value,"Apellidos":Apellidos.value,"DUI":DUI.value,"email":email.value,"telefono":telefono.value,"genero":genero.value},
				success:function(response,s,xhr){
					if(xhr.status === 200){
						toastr.success('Datos actualizados');
					}
				},
				error:function(data){
					if(data.status === 400){
						toastr.success('Error en actualizar datos');
					}
				}
			})
			saveButton.hide();
			editButon.show()
		})

		showModalButton.on("click",function(){
			modalImage.modal('show')
		})
	})
	
</script>
@endsection