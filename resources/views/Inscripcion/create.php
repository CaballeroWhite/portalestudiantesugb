<?php
require_once APP_PATH.'/views/partials/header.php';
?>
<div class="row">
  <div class="col col-8">
    <div class="card">
      <div class="card-header">
          <h5 class="card-title float-left">Agregar participante</h5>
      </div>
      <div class="card-body">

        <form class="" action="index.html" method="post">
          <div class="row">
              <div class="col col-6 pr-1">
                <div class="form-group">
                  <label for="">Nombres</label>
                  <input type="text" name="nombres" class="form-control" placeholder="Nombres" value="">
                </div>
              </div>
              <div class="col col-6 pl-1">
                <div class="form-group">
                  <label for="">Apellidos</label>
                  <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" value="">
                </div>
              </div>
          </div>


          <div class="form-group">
            <label for="">Correo Electronico</label>
            <input type="text" name="correo" placeholder="Correo Electronico" class="form-control" value="">
          </div>


          <div class="row">
            <div class="col col-6 pr-1">
              <div class="form-group">
                <label for="">Fecha de Nacimiento</label>
                <input type="date" class="form-control" name="fechan" value="">
              </div>
            </div>
            <div class="col col-6 pl-1">
              <div class="form-group">
                <label for="">Genero</label>
                <select class="form-control" name="">
                  <option value="">Masculino</option>
                  <option value="">Femenino</option>
                  <option value="">Sin especificar</option>
                </select>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="">Nacionalidad</label>
            <select class="form-control" name="">
              <option value="">Salvadoreña</option>
              <option value="">Otra</option>
            </select>
          </div>

          <div class="row">
              <div class="col col-6 pr-1">
                <div class="form-group">
                  <label for="">DUI</label>
                  <input type="text" name="dui" class="form-control" placeholder="DUI" value="">
                </div>
              </div>
              <div class="col col-6 pl-1">
                <div class="form-group">
                  <label for="">NIT</label>
                  <input type="text" name="nit" class="form-control" placeholder="NIT" value="">
                </div>
              </div>
          </div>

          <div class="form-group">
            <label for="">Telefono</label>
            <input type="text" name="telefono" class="form-control" placeholder="Telefono" value="">
          </div>

          <div class="form-group">
            <label for="">Departamento</label>
            <input type="text" class="form-control" name="" placeholder="Departamento" value="">
          </div>

          <div class="form-group">
            <label for="">Municipio</label>
            <input type="text" class="form-control" name="" placeholder="Municipio" value="">
          </div>

          <input type="submit" class="btn btn-success" name="" value="Guardar">

        </form>

      </div>
    </div>
  </div>
</div>





<?php
require_once APP_PATH.'/views/partials/footer.php';
?>
