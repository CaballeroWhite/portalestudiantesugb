@extends('layouts.app')


@section('content')
<div class="row">
  <div class="col-12 col-md-10 " style="border-left: 1px solid #eee;border-right: 1px solid #eee;">
    <div class="col-12">

      <div class="row">
        <div class="col-12 border-bottom">
          <h5 class="card-title float-left">Cursos Disponibles</h5>
        </div>
        <div class="col-12">
          <p class="m-2">Filtrado de categorias</p>
          <div class="form-group">
            <select class="form-control" name="Especialidad" id="Especialidad">
              <option>
                Mostrar-Todo
              </option>
              @foreach ($Especialidades as $Especialidad)
              <option>
                <?= $Especialidad ?>
              </option>
              @endforeach
            </select>
          </div>
        </div>
      </div>

      <div class="row" id="cursosAll">
      @foreach($Cursos as $curso)
        <div class='col-12 col-md-4'>
          <a href='/cursos/detalle/<?php echo $curso->nombre ?>' class='w-100'>
            <div class='card shadow rounded'>

              <img class="card-img-top" style="height: 11rem; object-fit: cover;" src='<?= asset("images/Banners/Cursos/")?>/<?= $curso->banner ?>' alt="Card image cap">
              <div class="card-body">
                <div>
                  <h6 class="text-navy"><?= $curso->nombre ?></h6>
                  <button class="btn btn-sm text-gray float-right"><i class="fas fa-angle-right"></i></button>
                </div>
                <span class="text-muted"><?= $curso->especialidad ?></span>
              </div>
            </div>
          </a>
        </div>
      @endforeach
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  let Especialidades = document.getElementById("Especialidad");
  let cursosFiltrado = document.getElementById("cursosAll");
  let templateCursos = ``;



  Especialidades.addEventListener('click', (e) => {
    let selectedValue = Especialidades.options[Especialidades.selectedIndex].value;

    getCursosEspecialidad(selectedValue);
  });


  function getCursosEspecialidad(value) {
    templateCursos = ``;

    $.ajax("/explorar/categorias", {
      method: "POST",
      data: {
        "_token": "{{ csrf_token() }}",
        "Especialidad": value,
      },
      success: function(response) {

        let data = JSON.parse(response)
        for (let i = 0; i < data.length; i++) {

          templateCursos += `
        <div class='col-12 col-md-4'>
          <a href='/cursos/detalle/${data[i]["nombre"]}'  class='w-100'>
            <div class='card shadow rounded'>
            
                <img class="card-img-top" style="height: 11rem; object-fit: cover;"
                      src="<?= asset("images/Banners/Cursos/")?>/${data[i]["banner"]}" alt="Card image cap">
                  <div class="card-body">
                    <div>
                      <h6 class="text-navy">${data[i]["nombre"]}</h6>
                      <button class="btn btn-sm text-gray float-right"><i class="fas fa-angle-right"></i></button>
                    </div>
                    <span class="text-muted">${data[i]["especialidad"]}</span>
                  </div>
            </div>
          </a>
        </div>
        `;
          cursosFiltrado.innerHTML = templateCursos;
        }

      }
    })

  }
</script>
@endsection