@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12 col-md-8" style="border-left: 1px solid #eee;border-right: 1px solid #eee;">
        <div class="mr-auto p-2">
            <h4 class="ml-2">Participantes</h4>
        </div>

  
        @foreach($participantesCurso as $participante)

        <div class="card card-body">

            <h4 class="ml-2 "><?= $participante->nombres . " " . $participante->apellidos ?></h4>

        </div>
        @endforeach

    </div>
</div>
@endsection