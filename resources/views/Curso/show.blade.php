@extends('layouts.app')

@section('styles')
<style>
	.selected::after{
   
    	right: 2em !important;

    
	}	
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-12 col-md-8" style="border-left: 1px solid #eee;border-right: 1px solid #eee;">
		<!--<h2>Bienvenido al curso!</h2>
		<p>Todavia no hay contenido para mostrar.</p>-->
		<div class="card w-100">
			<div class="card-header">
				General
			</div>
			<div class="card-body">
				@if ($curso->banner)
				<img style="height: 189" src='<?= asset("images/Banners/Cursos/") ?>/<?= $curso->banner ?>' alt="Card image cap">
				@else
				<img src="{{ asset('/images/bannerejemplo-min.png') }}" height="189">
				@endif

			</div>
		</div>

		<!-- MOSTRAR ANUNCIOS -->
		@auth('tutor')
		@endauth

		@auth('participantes')
		@empty($estado)

		@endempty
		@if($estado == "inscrito")
		<div class="card w-100">
			<div class="card-header">
				Anuncios del docente
			</div>
			<div class="card-body">
			
				@foreach($grupos as $Grupo)
				
				<div class="select-box" data-grupoID="<?= $Grupo->id ?>">
					@foreach ($Grupo->getAnuncios as $anuncioC)


					<div class="row mr-2 ml-2 options-container">
						<a class="w-100" style="height: 70px;" onclick="showContent('<?= $anuncioC->Contenido ?>','<?= $anuncioC->id ?>')">
							<div class="card w-100 shadow-sm">
								<div class="card-body option" style="background-color: #f8f9fa">
									<div class="">
										<div class="row">

											<div class="pl-3">
												<h5><?= $anuncioC->Nombre ?></h5>
											</div>

										</div>

									</div>
								</div>
							</div>
						</a>

					</div>
					@endforeach
					
					<div class="row selected">
                                                <div class="row mr-2 ml-2 w-100" style="height: 70px;">
                                                    <div class="w-100" onclick="">
                                                        <div class="card w-100 shadow-sm">
                                                            <div class="card-body" style="background-color: #f8f9fa !important;">
                                                                <div class="row">

                                                                    <div class="pl-3">
                                                                        <h4><?= $Grupo->nombreGrupo ?></h4>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                                                          
                                                </div>
                                              
                                            </div>
				</div>
				@endforeach
			</div>
		</div>
		@endif
		@endauth



		<!-- -->
		@auth('tutor')

		@endauth

		@empty($estado)

		@endempty
		@if($estado == "inscrito")

		@else
		@auth('participantes')
		<div class="card w-100">
			<div class="card-header">
				Contenido del curso - Unidades
			</div>
			<div class="card-body">
				@foreach ($unidades as $Unidades => $Unidad)

				<div class="row border m-2 p-2 rounded shadow-sm">
					<h5 class="p-2 ml-4" style="color:brown"><?= $Unidad->nombre ?></h5>
				</div>

				@endforeach
			</div>
		</div>
		@endauth
		@endif




		<div class="card w-100">
			<div class="card-header">
				23 de marzo al 29 de marzo
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col col-12">
						<a href="#" class="text-secondary"><i class="far fa-comments"></i> Foro de Consultas</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- RIGHT SIDE -->
	<div class="col-12 col-md-4">
		<div class="card">
			<div class="card-header">
				@isset($curso)
				<h5><?= $curso->nombre; ?></h5>
				@endisset

			</div>
			<div>
				<div class="card-body">
					<!-- -->
					@auth('tutor')

					<div class="card-body p-0">
						<div class="navbar-sidebar2">
							<ul class="list-unstyled navbar__list">
								<li><a href="/cursos/personas/<?= str_replace(" ", "-", $curso->nombre); ?>"><i class="fas fa-users"></i>Alumnos</a></li>
								<li><a href="/cursos/gestionCurso/<?= $curso->id ?>"><i class="fas fa-folder"></i>Gestion de Inicio</a></li>
							</ul>
						</div>
					</div>
					@endauth
					@empty($estado)

					@endempty

					@auth('participantes')
					@switch($estado)
					@case("pendiente")
					<button class="rounded bg-secondary text-white btn-block p-2">
						<li class="fas fa-exclamation-triangle float-left ml-2 mt-1"></li>Pendiente
					</button>
					@break
					@case("inscrito")
					<button class="rounded bg-primary text-white btn-block p-2">Inscrito</button>

					<div class="card-body p-0">
						<div class="navbar-sidebar2">
							<ul class="list-unstyled navbar__list">
								<li><a href="/cursos/personas/<?= str_replace(" ", "-", $curso->nombre); ?>"><i class="fas fa-users"></i>Personas</a></li>
								<li><a href="#"><i class="fas fa-clipboard-list"></i>Tareas</a></li>
								<li><a href="#"><i class="fas fa-folder"></i>Archivos</a></li>
							</ul>
						</div>
					</div>
					@break
					@default
					<button data-toggle="modal" data-target="#Inscripcion" class="rounded bg-success text-white btn-block p-2">Inscribirse</button>

					@endswitch
					@endauth
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="Inscripcion" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Inscripcion</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-12 text-center d-flex justify-content-center" style="height:2em">
					<li class="fas fa-exclamation-triangle float-left ml-2 mt-1"></li>
				</div>
				<div class="col-12">
					<p>Recuerde, que al confirmar su inscripción, <span class="text-danger">pasará a un estado de pendiente</span>, hasta que el encargado confirme su participación</p>
				</div>

			</div>
			<div class="modal-footer">
				<!-- Enviar al controller de cursos, el id del group-->
				<a href="/Inscripcion/grupo/curso/<?= $GrupoCurso[0]->id ?>"><button type="button" class="btn btn-primary">Confirmar inscripción</button></a>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>

<!-- Anuncio Modal description -->

<div class="modal fade" id="InfoAnuncio" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Contenido del anuncio</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-12 text-center d-flex justify-content-center" style="height:2em">
					<li class="fas fa-exclamation-triangle float-left ml-2 mt-1"></li>
				</div>
				<div class="col-12">
					<p id="anuncioContent">

					</p>
				</div>

			</div>

		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	function showContent(contenido) {
		let anuncioContent = $("#anuncioContent")
		let anuncioModal = $("#InfoAnuncio")
		anuncioModal.modal('show')
		anuncioContent.text(contenido)


	}
	const selected = document.querySelectorAll(".selected");
	const optionsContainer = document.querySelectorAll(".options-container");
	const selectBox = document.querySelectorAll(".select-box")

	const options = document.querySelectorAll(".options");

	selectBox.forEach((e) => {


		e.childNodes.forEach((hijo) => {
			let d = hijo.className

			if (String(d).includes("selected")) {
				hijo.addEventListener("click", (ev) => {
					e.childNodes.forEach((hijo2) => {
						let d = hijo2.className
						if (String(d).includes("options-container")) {
							hijo2.classList.toggle("active")
						}
					})

				})
			}
		})

	})
</script>
@endsection