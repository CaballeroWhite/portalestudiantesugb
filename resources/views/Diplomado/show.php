<?php
require_once APP_PATH . '/views/partials/header.php';
?>

<div class="row">
	<div class="col-12 col-md-8" style="border-left: 1px solid #eee;border-right: 1px solid #eee;">
		<!--<h2>Bienvenido al curso!</h2>
		<p>Todavia no hay contenido para mostrar.</p>-->
		<div class="card w-100">
			<div class="card-header">
				General
			</div>
			<div class="card-body">
				<img src="<?= ASSETS ?>/images/bannerejemplo-min.png" height="189">
			</div>
		</div>
		<div class="card w-100">
			<div class="card-header">
				<h5 class="text-center">Descripción</h5>
			</div>
			<div class="card-body">
				<p class="text-justify"><?= $Diplomado->descripcion?></p>
			</div>
		</div>
		<div class="card w-100">
			<div class="card-header">
				Contenido - Modulos
			</div>
			<?php
			foreach ($data["Modulos"] as $Modulos => $Modulo) {
			?>
				<div class="row border m-2 p-2 rounded shadow-sm flex">
					<div class="col-11">
						<h5 class="p-2 ml-4" style="color:brown"><?= $Modulo->nombre ?></h5>
					</div>
					<div class="col-1">
						<i class="fas fa-eye align-middle"></i>
					</div>
					<!-- <button data-toggle="modal" data-target="#InfoMod"></button> -->
				</div>
			<?php
			}
			?>
		</div>
		<div class="card w-100">
			<div class="card-header">
				23 de marzo al 29 de marzo
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col col-12">
						<a href="#" class="text-secondary"><i class="far fa-comments"></i> Foro de Consultas</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-12 col-md-4">
		<div class="card">
			<div class="card-header">
				<h5><?= $Diplomado->nombre ?></h5>
				
			</div>
			<div class="card-body p-0">
				<div class="navbar-sidebar2">
					<ul class="list-unstyled navbar__list">
						<li><a href="#"><i class="fas fa-users"></i>Personas</a></li>
						<li><a href="#"><i class="fas fa-clipboard-list"></i>Tareas</a></li>
						<li><a href="#"><i class="fas fa-folder"></i>Archivos</a></li>
					</ul>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="modal" tabindex="-1" id="InfoMod" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<?php
require_once APP_PATH . '/views/partials/footer.php';
?>