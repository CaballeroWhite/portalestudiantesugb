@extends('layouts.app')

@section('styles')
    <style>
        #calendarizacion-img {
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12 col-md-10">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <h5 class="card-title float-left">Calendarización del año</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="card">
                        <div class="card-body">
                           <div class="img-container">
                                <a href="<?= asset('images/calendarizacion.jpg')?>"><img src="<?= asset('images/calendarizacion.jpg')?>" id="calendarizacion-img" alt="" class="img-responsive"></a>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection