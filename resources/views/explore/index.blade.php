@extends('layouts.app')

@section('styles')
<style>
    .card-explorar {
        width: 250px;
    }
</style>
@endsection
@section('content')
<div class="row p-2 mb-3 border-bottom">
    <div class="col-12 d-flex">
        <div class="mr-auto p-2">
            <h4 class="ml-2">Cursos disponibles</h4>
        </div>

        <div class="p-2">
            <a href="/explorar/cursos">ver más</a>
        </div>

    </div>

</div>

<div class="row">
    <div id="cursos" class="col-sm-12 col-md-12">



        @foreach ($Cursos as $Curso)

        <div class="col-sm-2 col-md-4">
            <a href="/cursos/detalle/<?= str_replace(" ","-",$Curso->nombre); ?>" class="w-100">
                <div class="card shadow rounded card-explorar">

                    @if ($Curso->banner)
                    <img class="card-img-top" style="height: 11rem; object-fit: cover;"
                    src='<?= asset("images/Banners/Cursos/")?>/<?= $Curso->banner ?>' alt="Card image cap">
                    @else
                        
                        <div class="w-100" style="height: 11rem; background-color: #901014;"></div>
                    @endif

                    <div class="card-body p-3 pb-4">
                        <div>
                            <h6 class="text-navy"><?= $Curso->nombre ?></h6>
                            <button class="btn btn-sm text-gray float-right"><i class="fas fa-angle-right"></i></button>
                        </div>
                        <span class="text-muted"><?= $Curso->especialidad ?></span>

                    </div>
                </div>
            </a>
        </div>
        @endforeach


    </div>

</div>

<div class="row p-2 mb-3 border-bottom">
    <div class="col-12 d-flex">
        <div class="mr-auto p-2">
            <h4 class="ml-2">Diplomados disponibles</h4>
        </div>

        <div class="p-2">
            <a href="/">ver más</a>
        </div>

    </div>

</div>

<div class="row">
    <div id="diplomados" class="col-sm-12 col-md-12">

        @foreach($CatalogoDiplomados as $Diplomado)
            <div class="col-sm-2 col-md-4">
                <a href="/" class="w-100">
                    <div class="card shadow rounded">
                    <div class="w-100" style="height: 11rem; background-color: #901014;"></div>
                        <div class="card-body p-3 pb-4">
                            <div>
                                <h6 class="text-navy"><?= $Diplomado->nombre ?></h6>
                                <button class="btn btn-sm text-gray float-right"><i class="fas fa-angle-right"></i></button>
                            </div>
                            <span class="text-muted"><?= $Diplomado->especialidad ?></span>

                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>






@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous"></script>
<script>
    $(
        $('#cursos').slick({
            autoplay: true,
            slidesToShow: 2,
            autoplaySpeed: 3000,
            dots: true,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 4,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                        dots: true
                    }
                }
            ]
        }),

        $('#diplomados').slick({
            autoplay: true,
            slidesToShow: 4,
            autoplaySpeed: 3000,
            dots: true,
            responsive: [{
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                        dots: true
                    }
                }
            ]
        })
    );
</script>
@endsection