

<!--$path = URL_PATH -->
@extends('layouts.app')
@section('styles')

<style>
    .card-info.card-outline {
        border-top: 3px solid #b52d3a;
    }

    .card-header {
        background-color: transparent;
    }

    .nav-link {
        color: #962222 !important;
    }

    .btnNotice {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 50px;
        height: 40px;
        box-shadow: 3px 3px 0px 0px rgba(181, 45, 58, 1);
        border-radius: 5px;
        cursor: pointer;
    }

    .btnNotice--delete {
        margin-left: 5px;
        margin-top: 10px;
    }
</style>
@endsection


@section('content')
<div class="card card-info card-outline">
    <div class="card-header">
        <div class="card-title text-center">
            <h4 class="align-middle"><?= $curso->nombre ?></h4>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- START PANTALLAS -->
                <div class="row">
                    <div class="col-sm-12 col-lg-9">
                        <div class="tab-content">
                            <div role="tabpanel" id="Principal" class="active show tab-pane fade">
                                <div class="row">
                                    <div class="col-lg-7 col-sm-12 text-center">
                                        <h3>Principal</h3>
                                    </div>
                                    <div class="col-lg-4 col-sm-12 text-center">
                                        <!--<button class="btn btn-primary" data-toggle="modal" data-target="#contenidoModal"><i class="fas fa-plus mr-1"></i>Agregar Contenido</button>-->
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#createGrupoModal"><i class="fas fa-plus mr-1"></i>Crear grupo de anuncios</button>
                                    </div>
                                </div>
                                <hr>
                                <h4><?= $grupos ?></h4>
                                @foreach($grupos as $Grupo)
                               
                                
                                    <div class="select-box" data-grupoID="<?= $Grupo->id ?>">

                                        
                                        @foreach ($Grupo->getAnuncios as $anuncioC)
                                        
                                            <div class="row mr-2 ml-2 options-container">
                                                <a class="w-75" style="height: 70px;" onclick="showContent('<?= $anuncioC->Contenido ?>','<?= $anuncioC->id ?>')">
                                                    <div class="card w-100 shadow-sm">
                                                        <div class="card-body option" style="background-color: #f8f9fa">
                                                            <div class="">
                                                                <div class="row">

                                                                    <div class="pl-3">
                                                                        <h5><?= $anuncioC->Nombre ?></h5>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="btnNotice btnNotice--delete" onclick="onclickDeleteNotice('<?= $anuncioC->id ?>')">
                                                    <i class="far fa-trash-alt"></i>
                                                </div>
                                            </div>
                                        
                                        @endforeach

                                            <div class="row selected">
                                                <div class="row mr-2 ml-2 w-100" style="height: 70px;">
                                                    <div class="w-75" onclick="">
                                                        <div class="card w-100 shadow-sm">
                                                            <div class="card-body" style="background-color: #f8f9fa !important;">
                                                                <div class="row">

                                                                    <div class="pl-3">
                                                                        <h4><?= $Grupo->nombreGrupo ?></h4>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                    <div class="w-25 text-center d-flex justify-content-center align-items-center">
                                                        <div class="card col-6 h-50 d-flex justify-content-center" onclick="showModalAnuncio('<?= $Grupo->id ?>')">
                                                            <button data-toggle="modal" data-target="#contenidoModal"><i class="fas fa-plus-circle"></i></button>
                                                        </div>
                                                            
                                                    </div>                                        
                                                </div>
                                              
                                            </div>
                                        
                                    </div>
                                @endforeach
                            </div>
                            <div role="tabpanel" id="tareas" class="tab-pane fade">
                                <div class="col-12 text-center">
                                    <h3>Tareas</h3>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END Pantallas-->
                    <div class="col-lg-3 col-sm-12">
                        <div role="tabpanel" role="tablist">

                            <di class="nav nav-tabs flex-column" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" href="#Principal" aria-controls="Principal" data-toggle="pill" role="tab" aria-selected="true">Página de inicio</a>
                                <a class="nav-link" href="#tareas" aria-controls="tareas" data-toggle="pill" role="tab">Tareas</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

</div>


<!-- MODAL ANUNCIOS-->

<div class="modal fade" id="contenidoModal" tabindex="-1" role="dialog" aria-labelledby="contenidoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="contenidoModalLabel">Agregar contenido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group m-2 w-100">
                        <label>Nombre del anuncio</label>
                        <input type="text" name="anuncio" id="anuncio" class="form-control w-100" placeholder="">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group m-2 w-100">
                        <label for="" class="text-danger">Archivo opcional</label>
                        <input type="file" name="fileAnuncio" id="fileAnuncio" class="form-control w-100">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group m-2 w-100">
                        <label for="">Contenido del anuncio</label>
                        <textarea name="contenido" id="contenido" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="anuncio()">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL CREAR GRUPO-->

<div class="modal fade" id="createGrupoModal" tabindex="-1" role="dialog" aria-labelledby="createGrupoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="createGrupoModalLabel">Crear Grupo de anuncios</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group m-2 w-100">
                        <label>Nombre del grupo</label>
                        <input type="text" name="nameGrupo" id="nameGrupo" class="form-control w-100" placeholder="">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="createGroup('<?= $curso->id ?>')">Guardar</button>
            </div>
        </div>
    </div>
</div>



<!-- MODAL CAMBIO DE CONTENIDO DE UN ANUNCIO -->

<div class="modal fade" id="actualizarModal" tabindex="-1" role="dialog" aria-labelledby="actualiarModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="actualiarModalLabel">Editar contenido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group m-2 w-100">
                        <label for="">Contenido del anuncio</label>
                        <textarea name="contenidoUpdate" id="contenidoUpdate" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="text" id="idAnuncio" style="display: none;" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btnCambios">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="/js/gestionCurso.js"></script>
<script>

let anuncioId = 0


function showModalAnuncio(grupoAnuncioId){
    anuncioId  = grupoAnuncioId
}

function anuncio(){
    createNotice('<?= $curso->id ?>',anuncioId)
}
</script>

<script>
    
    var idAnuncio = document.getElementById("idAnuncio")
    var btnCambios = $("#btnCambios")

    function showContent(contenido,id){

        //idAnuncio.Value = id
        let anuncioContent = $("#contenidoUpdate")
        let anuncioModal = $("#actualizarModal")
        anuncioModal.modal('show')
        anuncioContent.text(contenido)

        btnCambios.click(function(e){
            updateContentNotice(id)
        })
    }


</script>
@endsection