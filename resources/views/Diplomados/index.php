<?php
require_once APP_PATH . '/views/partials/header.php';
?>
<div class="row">
  <div class="col-12 col-md-10 " style="border-left: 1px solid #eee;border-right: 1px solid #eee;">
      <div class="col-12">
          
        <div class="row">
          <div class="col-12 border-bottom">
            <h5 class="card-title float-left">Diplomados Disponibles</h5>
          </div>
          <div class="col-12">
            <p class="m-2">Filtrado de categorias</p>
            <div class="form-group">
              <select class="form-control">
                <?php foreach($data["Especialidades"] as $Diplomados){ ?>
                <option>
                  <?= $Diplomados ?>
                </option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
          

          
            <div class="row">
            <?php foreach($data["Diplomados"] as $Diplomados => $Diplomado) {?>
                <div class="col-12 col-md-4">
                  <a href="<?= $this->router("Diplomados.show", $Diplomado->id);?>" class="w-100">
                    <div class="card shadow rounded">
                    <?php if($Diplomado->banner){ ?>
                        <img class="card-img-top" style="height: 11rem; object-fit: cover;"
                        src="<?= CENTRAL_PATH.'/public/images/Banners/Diplomados/'.$Diplomado->banner ?>" alt="Card image cap">
                        <?php } else { ?>
                        <div class="w-100" style="height: 11rem; background-color: #901014;"></div>
                        <?php } ?>
                      <div class="card-body">
                        <div>
                          <h6 class="text-navy"><?= $Diplomado->nombre ?></h6>
                          <button class="btn btn-sm text-gray float-right"><i class="fas fa-angle-right"></i></button>
                        </div>
                        <span class="text-muted"><?= $Diplomado->especialidad ?></span>
                      </div>
                    </div>
                  </a>
                </div>
            <?php }?>
            </div>
        
        
      </div>
  </div>
</div>





<?php
require_once APP_PATH . '/views/partials/footer.php';
?>