<?php
require_once APP_PATH.'/views/partials/header.php';
?>
<div class="row">
  <div class="col col-12">
    <div class="card">
      <div class="card-header">
          <h5 class="card-title float-left">Seleccione un periodo</h5>
          <a href="#" class="btn btn-sm btn-success float-right">Agregar</a>
      </div>
      <div class="card-body">


        <ul class="list-group">
          <li class="list-group-item"><a href="#">2019</a></li>
          <li class="list-group-item"><a href="#">2018</a></li>
          <li class="list-group-item"><a href="#">2017</a></li>
          <li class="list-group-item"><a href="#">2016</a></li>
          <li class="list-group-item"><a href="#">2015</a></li>
        </ul>

      </div>
    </div>
  </div>
</div>





<?php
require_once APP_PATH.'/views/partials/footer.php';
?>
