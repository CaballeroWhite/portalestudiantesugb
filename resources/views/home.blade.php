@extends('layouts.app')

@section('content')
<div class="container">
    @auth('tutor')
    <div class="row mb-3">
        <h2 class="ml-2">Cursos que impartes</h2>
    </div>
    @endauth
    
    @auth('participantes')
    <div class="row mb-3">
        <h2 class="ml-2">Tus grupos y cursos</h2>
    </div>
    @endauth
    
    <div class="row">
    @foreach ($Cursos as $curso)
    <div class="col-12 col-md-4">
        <a href="/cursos/detalle/<?= str_replace(" ","-",$curso->curso->nombre); ?>">
            <div class="card shadow rounded">
                @if ($curso->curso->banner)
                <img class="card-img-top" style="height: 11rem; object-fit: cover;"
                  src='<?= asset("images/Banners/Cursos/")?>/<?= $curso->curso->banner ?>' alt="Card image cap">
                @else
                    
                    <div class="w-100" style="height: 11rem; background-color: #901014;"></div>
                @endif
                


                <div class="card-body p-3 pb-4">
                    <div>
                        <h5 class="text-navy"><?= $curso->curso->nombre ?></h5>
                        <button class="btn btn-sm text-gray float-right"><i class="fas fa-angle-right"></i></button>
                    </div>

                </div>
            </div>
        </a>

    </div>
    @endforeach
    </div>
    <div class="row mb-3">
        @auth('participantes')
            <h2 class="ml-2">Tus Diplomados</h2>
        @endauth	
        @auth('tutor')
        <h2 class="ml-2">Módulos que impartes</h2>
        @endauth
    </div>

    <div class="row">
    @foreach ($modulos as $data)
    <div class="col-12 col-md-4">
        <a href="/modulos/unidades/<?= $data->modulo->id ?>">
            <div class="card shadow rounded">

                <div class="w-100" style="height: 11rem; background-color: #901014;"></div>


                <div class="card-body p-3 pb-4">
                    <div>
                        <h5 class="text-navy"><?= $data->modulo->nombre ?></h5>
                        <button class="btn btn-sm text-gray float-right"><i class="fas fa-angle-right"></i></button>
                    </div>

                </div>
            </div>
        </a>

    </div>
    @endforeach
    </div>
    <!-- <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif




                </div>
            </div>
        </div>
    </div> -->
</div>
@endsection