<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon_continua.png')}}">

    <!-- Title Page-->
    <title>Continua</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Fontfaces CSS-->
    <link href="{{ asset('css/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{asset('/vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">
    <!-- Vendor CSS-->
    <link href="{{ asset('vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">


    <!-- Main CSS-->
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('css/gruposAnuncios.css') }}" rel="stylesheet" media="all">

    <!-- slick jquery library-->
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" integrity="sha512-wR4oNhLBHf7smjy0K4oqzdWumd+r5/+6QO/vDda76MW5iug4PT7v86FoEkySIJft3XA0Ae6axhIvHrqwm793Nw==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css" integrity="sha512-6lLUdeQ5uheMFbWm3CP271l14RsX1xtx+J5x2yeIDkkiBpeVTNhTqijME7GgRKKi6hCqovwCoBTlRBEC20M8Mg==" crossorigin="anonymous" />
    

    <!-- Toastr -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />

  
    @yield('styles')
</head>

<body class="animsitionn">
    <div class="page-wrapper">
        <header class="header-desktop4">
            <div class="container">
                <div class="header4-wrap">
                    <div class="header__logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('images/logo_continua.png') }}" style="height:50px;" alt="CoolAdmin" />
                        </a>
                    </div>
                    @auth('tutor')
                    <div class="header__tool">
                        <div class="float-left">
                            <h3>Panel del docente</h3>
                        </div>
                    </div>
                    @endauth

                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src="https://www.gravatar.com/avatar?d" alt="John Doe" />
                                </div>
                                <div class="content">
                                    @auth('participantes')
                                        <a class="js-acc-btn" href="#">{{ Auth::guard('participantes')->user()->nombres }}</a>
                                    @endauth
                                    @auth('tutor')
                                        <a class="js-acc-btn" href="#">{{ Auth::guard('tutor')->user()->tutor->nombres }}</a>
                                    @endauth
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="info clearfix">
                                        <div class="image">
                                            <a href="#">
                                                <img src="{{ asset('images/default-avatar.png') }}" alt="Admin" />
                                            </a>
                                        </div>
                                        <div class="content">
                                            <h5 class="name">
                                            @auth('participantes')
                                                <a href="#">{{ Auth::guard('participantes')->user()->nombres }}</a>
                                            @endauth
                                            @auth('tutor')
                                                <a href="#">{{ Auth::guard('tutor')->user()->tutor->nombres }}</a>
                                            
                                            @endauth
                                            </h5>
                                            @auth('participantes')
                                                <span class="email">{{ Auth::guard('participantes')->user()->correo }}</span>
                                            @endauth
                                            @auth('tutor')
                                                <span class="email">{{ Auth::guard('tutor')->user()->tutor->correo }}</span>
                                            @endauth
                                        </div>
                                    </div>
                                    <div class="account-dropdown__body">
                                        <div class="account-dropdown__item">
                                            <a href="/cuenta">
                                                <i class="zmdi zmdi-account"></i>Cuenta</a>
                                        </div>
                                        <div class="account-dropdown__item">
                                            <a href="#">
                                                <i class="zmdi zmdi-money-box"></i>Pagos</a>
                                        </div>
                                    </div>
                                    <div class="account-dropdown__footer">
                                        <a href="">
                                            <i class="zmdi zmdi-power"></i>Salir</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    

    <section class="p-t-30 p-b-70">
        <div class="container">
            <div class="row">
                <div class="col-xl-3">
                    <!-- MENU SIDEBAR-->
                    <aside class="menu-sidebar3 js-spe-sidebar">
                        <nav class="navbar-sidebar2 navbar-sidebar3">
                            <ul class="list-unstyled navbar__list">
                                <li class="active has-sub">
                                    <a class="js-arrow" href="#">
                                        <i class="fas fa-tachometer-alt"></i>Navegacion
                                        <span class="arrow">
                                            <i class="fas fa-angle-down"></i>
                                        </span>
                                    </a>
                                    <ul class="list-unstyled js-sub-list">

                                        <li>
                                            <a href="{{ route('home') }}"><i class="fas fa-home"></i>Inicio</a>
                                        </li>

                                        @auth('participantes')
                                            <li>
                                                <a href="/explorar"><i class="fas fa-binoculars"></i>Explorar</a>
                                            </li>
                                        @endauth
                                      

                                    </ul>
                                    <ul class="list-unstyled js-sub-list">
                                        <li><a href="/calendarizacion"><i class="fas fa-calendar-plus"></i>Calendarización</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                    </aside>
                    <!-- END MENU SIDEBAR-->
                </div>
                <div class="col-xl-9">
                    <div class="page-content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </section>

       
    

    <!-- Jquery JS-->
    <script src="{{ asset('vendor/jquery-3.2.1.min.js') }}"></script>
    
   
    <!-- Bootstrap JS-->
    <script src="{{ asset('vendor/bootstrap-4.1/popper.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-4.1/bootstrap.min.js') }}"></script>
    
    <script src="{{ asset('vendor/wow/wow.min.js') }}"></script>
    <script src="{{ asset('vendor/animsition/animsition.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>

    <script src="{{ asset('vendor/counter-up/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('vendor/counter-up/jquery.counterup.min.js') }}"></script>
    
    <script src="{{ asset('vendor/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{ asset('vendor/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>


    <!-- Main JS-->
    <script src="{{ asset('js/main.js') }}"></script>
    
    @yield('scripts')
    
</body>

</html>