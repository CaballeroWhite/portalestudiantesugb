var textAreaContent = document.querySelector("#contenidoUpdate")


var newContent = ""

function createNotice(cursoId,grupoNoticiaId) {
   
    let anuncio = $('#anuncio').val()
    let contenido = $('#contenido').val()

    $.ajax(`/gruposAnuncios/Anuncio/create/${anuncio}/${contenido}/${cursoId}/${grupoNoticiaId}`, {
        method: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(response,s,xhr) {
            if (xhr.status === 200) {
                alert("Nuevo anuncio creado con éxito")
                location.reload()
            } else if(xhr.status === 400){
                alert("Falla en la creación del anuncio")
                location.reload()
            }
        },
        error:function(){
            alert("Falla en la creación")
            location.reload()
        }
    })

}

function createGroup(cursoId) {
    let nombreGrupo = $('#nameGrupo').val();

    $.ajax(`/gruposAnuncios/create/${cursoId}`,{
        method:"POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{"nombreGrupo":nombreGrupo},
        success:function(response,s,xhr){
            
            if (xhr.status === 200) {
                alert("Nuevo grupo creado con éxito")
                location.reload()
            } else if(xhr.status === 400){
                alert("Falla en la creación del grupo")
                location.reload()
            }
        },
        error:function(){
            alert("Falla en la creación del grupo")
            location.reload()
        }
    })
   
}

function changeContent(path,AnuncioId) {

    var idAnuncio = document.getElementById("idAnuncio")

    $.get(`${path}/Anuncio/getAnuncio/${AnuncioId}`, data => {
        let Anuncio = JSON.parse(data)

        textAreaContent.value = ""
        textAreaContent.value = Anuncio.Contenido
        idAnuncio.innerHTML = Anuncio.id
        $("#actualizarModal").modal('show')
    })

}

textAreaContent.onkeyup = () => {
    newContent = textAreaContent.value
    textAreaContent.innerHTML = newContent
}

function updateContentNotice(idAnuncio) {
   //var idAnuncio = document.getElementById("idAnuncio")
    
 
    $.ajax(`/gruposAnuncios/Anuncio/edit/${idAnuncio}`, {
        method:"POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {"contenido":newContent},
        success:function(response,s,xhr) {
            if (xhr.status === 200) {
                alert("Anuncio actualizado")
                location.reload()
    
            } else if(xhr.status === 409){
                alert("Falla en la operación")
                location.reload()
            }
        },
        error:function(e){
            console.log(e)
            alert("Error en operación")
            location.reload()
        }
    })


}

const selected = document.querySelectorAll(".selected");
const optionsContainer = document.querySelectorAll(".options-container");
const selectBox = document.querySelectorAll(".select-box")

const options = document.querySelectorAll(".options");

selectBox.forEach((e) => {


    e.childNodes.forEach((hijo) => {
        let d = hijo.className

        if (String(d).includes("selected")) {
            hijo.addEventListener("click", (ev) => {
                e.childNodes.forEach((hijo2) => {
                    let d = hijo2.className
                    if (String(d).includes("options-container")) {
                        hijo2.classList.toggle("active")
                    }
                })

            })
        }
    })

})


function onclickDeleteNotice(noticeId) {

    let idNotice = noticeId

    $.ajax(`/gruposAnuncios/Anuncio/delete/${idNotice}`, {
        method:"POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(response,s,xhr) {
            if (xhr.status == 200) {
                alert("Anuncio eliminado")
                location.reload()
    
            } else if(xhr.status === 409){
                alert("Falla en la operación")
                location.reload()
            }
        },
        error:function(){
            alert("Error en operación")
            location.reload()
        }
    })

}