<?php

//Rutas de Explorar
Route::group(["prefix" => "explorar"],function(){
    /* Ver mas cursos */
    Route::get('/','ExplorarController@index');
    Route::get('/cursos','CursosController@index');
    /* Fin ver mas cursos*/
    //Ruta para filtrar los cursos por categoria
    Route::post('/categorias','CursosController@getCursos');
});

Route::get('/cursos/detalle/{curso}','CursosController@show');
Route::get('/cursos/personas/{curso}','CursosController@personas');
Route::get('/cursos/gestionCurso/{cursoid}','CursosController@GestionCurso');



