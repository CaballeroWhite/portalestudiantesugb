<?php

Route::group(["prefix" => "modulos"],function(){
    Route::get('/','DiplomadosController@index');
    Route::get('/unidades/{moduloId}','ModuloController@unidades');
});