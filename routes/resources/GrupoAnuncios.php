<?php

Route::group(["prefix" => "gruposAnuncios"],function(){
    Route::post('/create/{cursoId}','gruposAnunciosController@create');
    Route::post('/Anuncio/create/{anuncio}/{contenido}/{cursoId}/{grupoNoticiaId}','AnuncioController@create');
    Route::post('/Anuncio/delete/{idNotice}','AnuncioController@delete');
    Route::post('/Anuncio/edit/{idAnuncio}','AnuncioController@update');
});