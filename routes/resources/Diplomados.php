<?php

Route::group(["prefix" => "diplomados"],function(){
    Route::get('/','DiplomadosController@index');
    Route::get('/{diplomadoId}','DiplomadosController@show');
});