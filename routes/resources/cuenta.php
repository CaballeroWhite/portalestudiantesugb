<?php

Route::group(["prefix" => "cuenta"],function(){
    Route::get('/','CuentaController@index');
    Route::post('/updatePassword','CuentaController@updatePassword');
    Route::post('/updateUsuario','CuentaController@updateUsuario');
});