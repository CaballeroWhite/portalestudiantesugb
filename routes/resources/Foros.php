<?php

Route::group(["prefix" => "foros"],function(){
    Route::get('/','ForosController@index');
    Route::get('/{foroId}','ForosController@show');
});