<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/','HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('tutor-login','Auth\LoginTutorController@showLoginForm');
Route::post('tutor-login',['as'=>'tutor-login','uses'=>'Auth\LoginTutorController@login']);

require_once 'resources/cursos.php';
require_once 'resources/inscripcion.php';
require_once 'resources/cuenta.php';
require_once 'resources/GrupoAnuncios.php';
require_once 'resources/Calendarizacion.php';
require_once 'resources/Diplomados.php';
require_once 'resources/Modulo.php';
require_once 'resources/Foros.php';