<?php

 

namespace App;

 
use App\Models\Tutor;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;


class UsuarioTutor extends Authenticatable

{

    use Notifiable;

    protected $table = "usuariotutores";

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'username', 'password',

    ];

 
    public function tutor(){
        return $this->belongsTo(Tutor::class);
    }
    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'password',

    ];

}