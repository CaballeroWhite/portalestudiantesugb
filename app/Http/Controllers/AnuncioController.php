<?php 
namespace App\Http\Controllers;

use App\Models\CursoAnuncio;

class AnuncioController extends Controller{

    function create($anuncio,$contenido,$idCurso,$grupoAnunciosId){

        $NombreAnuncio = $anuncio;
        $ContenidoAnuncio = $contenido;

        $cursoA = CursoAnuncio::create([
            "Nombre" => $NombreAnuncio,
            "Contenido" => $ContenidoAnuncio,
            "Curso_id" =>  $idCurso,
            "grupoAnunciosId" => $grupoAnunciosId
        ]);
        

        if($cursoA->save()){
            //Creacion realizada con éxito
            return response('Datos actualizados', 200)->header('Content-Type', 'text/plain');	
        }else{
            return response('Datos actualizados', 400)->header('Content-Type', 'text/plain');	
        }

    
    }

    //Para el index, para mostrar el anuncio en el modal de editar
    function getAnuncio($id){

        $anuncio = CursoAnuncio::find($id);

        echo json_encode($anuncio);
    }


    function update($anuncioId){

        $contenido = $_POST["contenido"];
        
        $anuncioToUpdate = CursoAnuncio::where('id',intval($anuncioId))->update(["Contenido" => $contenido]);

        if($anuncioToUpdate == 1){
            //Update realizado con éxito
            return response('Anuncio actualizado', 200);	
        }else{
            return response('No updated', 409);	
        }
        
        
    }

    function delete($noticeId){
       // $deleteAnuncio = CursoAnuncio::find($noticeId);
        $result = null;

        if(CursoAnuncio::find($noticeId)->delete()){
            return response('Anuncio borrado', 200)->header('Content-Type', 'text/plain');	
        } else {
            return response('No deleted', 409)->header('Content-Type', 'text/plain');	
        }

    }
}





?>