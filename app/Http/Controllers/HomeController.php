<?php

namespace App\Http\Controllers;

use App\Models\Tutor;
use App\UsuarioTutor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Participante;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:participantes,tutor');
        //$this->middleware('auth:tutor');
       
       
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $dataCursos = [];
        $modulos = [];

        if(Auth::guard('participantes')->check()){
            $codigo = Auth::guard('participantes')->user()->codigo;
            $participante = Participante::where('codigo','=',$codigo)->first();

            $dataCursos = $participante->gruposCurso;
            $modulos = $participante->gruposDiplomado;
        }else if(Auth::guard('tutor')->check()){
            $idTutor = Auth::guard('tutor')->user()->tutor_id;
            $tutor = Tutor::where('id','=',$idTutor)->first();
            $dataCursos = $tutor->cursoGrupos;
        }
        
        

        return view('home',['Cursos' => $dataCursos,'modulos' => $modulos]);
        
    }
}
