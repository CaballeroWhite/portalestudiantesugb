<?php
namespace App\Http\Controllers;
use App\Models\Curso;
use App\Models\CursoGrupo;
use App\Models\CursoUnidad;
use App\Models\CursoInscripcion;
use App\Models\GrupoAnuncio;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CursosController extends Controller
{
  public function index(){
	$Cursos = Curso::get();
	$Especialidad = array();

	foreach($Cursos as $Data){
		array_push($Especialidad,$Data->especialidad);
	}
	
	$data = [
		"Cursos" => $Cursos,
		"Especialidades" => array_unique($Especialidad)
	];
    return view('Cursos.index',$data);
	
  }

  //Para Ajax
  public function getCursos(Request $request){

	//$Especialidad = $_POST["Especialidad"];
	$data = $request->all();
	$Especialidad = $data["Especialidad"];
	if($Especialidad === "Mostrar-Todo"){
		return json_encode(Curso::get());
	}else{
		return json_encode(Curso::where('especialidad','=',$Especialidad)->get());
	}
	
	// return response()->json($Especialidad);
  }
  

  function checkUserEstado($grupoId){
	if(Auth::guard('participantes')->check()){
		$id = Auth::guard('participantes')->user()->id;
		$estado = CursoInscripcion::where('participante_id', $id)->where('curso_grupo_id', $grupoId[0]->id)->get();

		if($estado->isEmpty()){
			return [];
		}else{
			return $estado[0]->estado;
		}
		
		

		
	}
	
  }
  function show($slug)
	{
		$cursoName = str_replace("-"," ",$slug);
	
		$cursoGetIdByName = Curso::where('nombre','=',$cursoName)->get()->first();
		$id = $cursoGetIdByName->id;
		
		//$grupo = CursoGrupo::find($id);
		$idGrupoCurso = CursoGrupo::where('curso_id',$id)->get();
		$curso = Curso::find($id);
		
	
		$UnidadesCurso = CursoUnidad::where('idcurso',$id)->get();
		$gruposCurso = GrupoAnuncio::where("Curso_id",'=',$curso->id)->get();
		$estado = $this->checkUserEstado($idGrupoCurso);

	
		//Grupoid, manda para buscar por id del grupo, en la tabla de inscripciones.
		if($idGrupoCurso == ""){
			$idGCurso = null;
		}else{
			$idGCurso = $idGrupoCurso;
		}
		
		return view('Curso.show', [
			'grupos' => $gruposCurso,
			'estado'		=> $estado,
			'curso' 		=> $curso,
			'unidades' 		=> $UnidadesCurso,
			'GrupoCurso' 		=> $idGCurso,
			'AnunciosCurso' => $curso->anuncios
		]);



	}

	 
	function personas($slug){
	
		$cursoName = str_replace("-"," ",$slug);
		$curso = Curso::where('nombre','=',$cursoName)->get()->first();
		//$id = $cursoGetIdByName->id;
		
		//$curso = Curso::find($id);
		
		$participantesCurso = DB::table('curso_grupo_participante')->join('participantes','curso_grupo_participante.participante_id','=','participantes.id')->where('curso_grupo_participante.curso_grupo_id','=',$curso->grupos[0]->id)->get();
		
		//$participantesCurso = CursoInscripcion::where('curso_grupo_id','=',$curso->grupos[0]->id)->get('participante_id');
		
		return view('Curso.personas',[
			'cursoinfo' 				=> $curso,
			'participantesCurso' 		=> $participantesCurso
		]);

		
	}


	function GestionCurso($cursoId){

		$curso = Curso::find($cursoId);
		$anunciosCurso  = Curso::find($cursoId)->anuncios;
		$gruposCurso = GrupoAnuncio::where("Curso_id",'=',$cursoId)->get();

		return view('GestionCurso.index',[
			"curso" => $curso,
			"anuncios" => $anunciosCurso,
			"grupos" => $gruposCurso
		]);
	}

	function delete($id){
		echo $id;

		
	}


}

?>
