<?php
// En este proyecto se hace uso del patron de diseño MVC
// la estructura de las url es la siguiente:
// http://ecugb.000webhostapp.com/(Nombre del Controlador)/(Nombre del metodo)
// Ejemplo http://ecugb.000webhostapp.com/Inscripcion/edit

// Se recomiendan usar los siguientes metodos:
// index: para mostrar el listado de registros
// create: para mostrar el formulario de registro
// store: para recibir los datos del formulario y guardarlos en la base de datos
// edit: para mostrar el formulario de edición
// update: para recibir los datos y actualizar la base de datos
// destroy: para eliminar registros de la base de datos

class Participantes extends Controller
{
  // Cada una de esta funciones es un metodo que puede se usado en las url
  public function create($errores = "[]"){

    // $this->view sirve para cargar la plantilla php dentro de la carpeta views
    // en este caso "Inscripción" es una subcarpeta y create hace referencia al
    // archivo create.php dentro de esa carpeta, notese que no es necesario escribir la extensión
    $errores = json_decode(urldecode($errores));

    $data = [
      "errores" => $errores
    ];
    $this->view('Participantes/create', $data); 
  }
  public function store(){
   
    // $_POST contiene todo los datos que recibe del formulario de inscripción
    // Para que se guarden sin espeficiar los campos los input del formulario deben
    // tener el atributo name con el mismo nombre del campo en la base de datos

    $validacion = $this->validate($_POST, [
      "nombres" => "required", 
      "apellidos" => "required",
      "correo" => "email", 
      "fecha_n" => "required",
      "genero" => "required",
      "ecivil" => "required",
      "nacionalidad" => "required",
      "dui" => "required | unique", 
      "nit" => "required |unique", 
      "telefono" => "required",
      "departamento" => "required",
      "municipio" => "required"

    ], Participante::class);
    if($validacion!==true){
      $this->redirect("Participantes.create", json_encode($validacion));
      return;
    }
    $newUser = Participante::create([
      "nombres"=>$_POST["nombres"],
      "apellidos"=>$_POST["apellidos"],
      "correo"=>$_POST["correo"],
      "fecha_n"=>$_POST["fecha_n"],
      "genero"=>$_POST["genero"],
      "ecivil"=>$_POST["ecivil"],
      "nacionalidad"=>$_POST["nacionalidad"],
      "dui"=>$_POST["dui"],
      "nit"=>$_POST["nit"],
      "telefono"=>$_POST["telefono"],
      "departamento"=>$_POST["departamento"],
      "municipio"=>$_POST["municipio"],
      "codigo"=>$this->generateCarnet(),
      "contrasena"=>"Continua.19"
    ]);

    $email = $this->library('Email');
    $template = $this->template('Inscripcion');
    $template = str_replace('%carnet%', $newUser->codigo, $template);
    $template = str_replace('%clave%', $newUser->contrasena, $template);
    $email->send($newUser->correo, 'Inscripción', $template);

    //header('Location: /ecugb/Participantes');
    $this->redirect('Participantes');
  }
  public function index(){
    $data = Participante::get();
    $this->view('Participantes/index', $data, 'Participantes');


  }
  // Esta función se encarga de la entrega de datos a la tabla de participantes
  // recibe los parametros del script de datatables
  public function api(){
    ## Read value
    $draw = $_POST['draw'];
    $row = $_POST['start'];
    $rowperpage = $_POST['length']; // Rows display per page
    $columnIndex = $_POST['order'][0]['column']; // Column index
    $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
    $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
    $searchValue = $_POST['search']['value']; // Search value

    $data = null;

    if ($searchValue!='') {
      $data = Participante::where("nombres", "LIKE", "%{$searchValue}%")->orWhere("codigo", "LIKE", "%{$searchValue}%")->orWhere("apellidos", "LIKE", "%{$searchValue}%")
            ->orWhereRaw("CONCAT(`nombres`, ' ', `apellidos`) LIKE ?", ['%'.$searchValue.'%']);
    } else {
      $data = Participante::select('*');
    }
    $totalRecords = Participante::get()->count();
    $totalRecordwithFilter = $data->get()->count();
    $data = $data->orderBy($columnName, $columnSortOrder);
    $data = $data->skip($row);
    $data = $data->take($rowperpage)->get();

    $response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecordwithFilter,
    "iTotalDisplayRecords" => $totalRecords,
    "aaData" => $data
    );

    echo json_encode($response);
  }
  
  
  public function show($id){
    $participante = Participante::find($id);

     $diplomados = array();
     $dataModulos = [];
     // Se agrupan todos los grupos por diplomado por modulo y campo nombre
    foreach($participante->gruposDiplomado as $grupo) {
      array_push($dataModulos,$grupo->modulo->nombre); //Obtener los modulos relacionados al participante
      $diplomados[$grupo->modulo->diplomado->nombre] [] = $grupo;
     }    
    
    $data = [
      "ModulosActivos"=> json_encode($dataModulos),
      "participante"=>$participante,
      "diplomados"=>$diplomados
    ];

    $this->view('Participantes/show', $data, "Participantes");
  }

  function edit($id, $errores = "[]"){
    $errores = json_decode(urldecode($errores)); 
    $participante = Participante::find($id);
    $data = [
        "participante" => $participante,
        "errores" => $errores 

    ];

    $this->view('Participantes/edit', $data);

  }
  function update($id){
    $validacion = $this->validate($_POST, [
      "nombres" => "required", 
      "apellidos" => "required",
      "correo" => "email", 
      "fecha_n" => "required",
      "genero" => "required",
      "ecivil" => "required",
      "nacionalidad" => "required",
      "dui" => "required | unique",
      "nit" => "required | unique", 
      "telefono" => "required",
      "departamento" => "required",
      "municipio" => "required" 
    ], Participante::class);
    if($validacion!==true){
      $this->redirect("Participantes.edit", $id,json_encode($validacion));
      return;
    }

    $participante = Participante::find($id);
    $participante->nombres = $_POST["nombres"];
    $participante->apellidos = $_POST["apellidos"];
    $participante->correo = $_POST["correo"];
    $participante->fecha_n = $_POST["fecha_n"];
    $participante->genero = $_POST["genero"];
    $participante->ecivil = $_POST["ecivil"];
    $participante->nacionalidad = $_POST["nacionalidad"];
    $participante->dui = $_POST["dui"];
    $participante->nit = $_POST["nit"];
    $participante->telefono = $_POST["telefono"];
    $participante->departamento = $_POST["departamento"];
    $participante->municipio = $_POST["municipio"];

    $participante->save();
    $this->redirect("Participantes.index");

}

  public function generateCarnet(){
    $prefix = "EC";
    $suffix = date("y");
    $cod = str_pad(rand(1,9999),4, "0", STR_PAD_LEFT);

    $carnet = $prefix.$cod.$suffix;

    if (!$this->checkCarnetAvalibity($carnet)) {
      return $carnet;
    } else {
      $this->generateCarnet();
    }
  }

  private function checkCarnetAvalibity($carnet){
    return (Participante::where('codigo', $carnet)->count()>0);
  }
  function inscribirDiplomado($participante){
    foreach ($_POST["modulos"] as $modulo) {
      DiplomadoInscripcion::create([
        'participante_id' => $participante,
        'diplomado_grupo_id' => $_POST["grupos"][$modulo],
        'estado' => 'activo'
      ]);
    }
    $this->redirect('Participantes.show', $participante);
  }
  function search($type, $term){
    $results = null;
    if (isset($type) and isset($term)) {
      switch ($type) {
        case 'nombre':
          $results = Participante::select("id","nombres", "apellidos", "codigo", "dui")
            ->where("nombres", "LIKE", "%{$term}%")->orWhere("apellidos", "LIKE", "%{$term}%")
            ->orWhereRaw("CONCAT(`nombres`, ' ', `apellidos`) LIKE ?", ['%'.$term.'%'])->take(5)->get();
          break;
        
        case 'codigo':
          $results = Participante::select("id","nombres", "apellidos", "codigo", "dui")
          ->where("codigo", "LIKE", "%{$term}%")->take(5)->get();
          break;

        case 'documento':
          $results = Participante::select("id","nombres", "apellidos", "codigo", "dui")
          ->where("dui", "LIKE", "%{$term}%")->take(5)->get();
          break;
      }
    }
    echo json_encode($results);
  }

 

}

?>
