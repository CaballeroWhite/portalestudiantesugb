<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginTutorController extends Controller
{
  
    

    use AuthenticatesUsers;


    protected $guard = 'tutor';

 

    /**

     * Where to redirect users after login.

     *

     * @var string

     */

    protected $redirectTo = '/home';

 

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {
        //$this->middleware('auth:tutor');
        $this->middleware('guest')->except('logout');

    }


    public function showLoginForm()

    {
        
        return view('auth.tutorLogin');

    }

    

    public function login(Request $request)

    {
        
        $credentials = ['username' => $request->usuario, 'password' => $request->contrasena];

        if(Auth::guard('tutor')->attempt($credentials)){
            auth()->guard('tutor');
            return redirect()->intended($this->redirectPath());
        }else{
            return back();
        }
      

       
        // if (auth()->guard('tutor')->attempt(['username' => $request->usuario, 'password' => $request->password])) {

        //     //dd(auth()->guard('tutor')->user());
        //     return redirect()->intended('home');
        // }

        

        //return back()->withErrors(['username' => 'Usuario or password are wrong.']);

    }
}