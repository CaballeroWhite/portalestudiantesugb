<?php
namespace App\Http\Controllers;

use App\Models\Curso;
use App\Models\ExplorarModel;


class ExplorarController extends Controller{

    public function __construct()
    {

        

    }

    public function index(){
        $ExplorarModel = new ExplorarModel();
        $CatalogoCursos = $ExplorarModel->getCursos();
        $CatalogoDiplomados = $ExplorarModel->getDiplomados();
        
        $data = [
            "Cursos" => $CatalogoCursos,
            "CatalogoDiplomados" => $CatalogoDiplomados
        ];

        return view('explore.index',$data);
        //$this->view("Explorar/index");

    }


}