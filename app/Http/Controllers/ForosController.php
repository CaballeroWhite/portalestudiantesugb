<?php

namespace App\Http\Controllers;
use App\Models\Foro;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ForosController extends Controller {
    //Mostrar los foros por fecha
    

    //Usuario puede inscribirse a un foro
    public function index(){
        //with, to make the relationship
        $Foros = Foro::with('tutor')->get();

        $data = [
            "count" => count($Foros),
            "rows" => $Foros
        ];

        return response()->json($data);
    }
}