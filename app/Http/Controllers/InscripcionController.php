<?php
/**
 *
 */
namespace App\Http\Controllers;
use App\Models\CursoInscripcion;

use Illuminate\Support\Facades\Auth;

class InscripcionController extends Controller
{
  public function create(){
    $this->view('Inscripcion/create');
  }
  
	public function save($cursoGrupoId){
    if(Auth::guard('participantes')->check()){
      $id = Auth::guard('participantes')->user()->id;
    }

		CursoInscripcion::create([
      "curso_grupo_id" => $cursoGrupoId,
      "participante_id" => $id,
      "estado" => "pendiente"
    ]);
    
    return redirect("home");

	}
}

?>
