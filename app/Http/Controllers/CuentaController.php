<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Tutor;
use App\Models\Participante;
use App\UsuarioTutor;

class CuentaController extends Controller
{
	
	function index()
	{
		return view('Cuenta/index');
	}
	public function updateUsuario(){
		// if(isset($_POST["idTutor"])){
		// 	$id = $_POST["idTutor"];
		// 	$update = Tutor::find($id);
		// }else{
		// 	$id = $_POST["idUsuario"];
		// 	$update = Participante::find($id);
		// }

		if(Auth::guard('participantes')->check()){
			$id = Auth::guard('participantes')->user()->id;
			$update = Participante::find($id);
		}

		if(Auth::guard('tutor')->check()){
			$id = Auth::guard('tutor')->user()->tutor_id;
			$update = Tutor::find($id);
		}

		$Nombres = $_POST["Nombres"];
		$Apellidos = $_POST["Apellidos"];
		$DUI = $_POST["DUI"];
		$email = $_POST["email"];
		$telefono = $_POST["telefono"];
		$genero = $_POST["genero"];

		
		$update->Nombres = $Nombres;
		$update->apellidos = $Apellidos;
		$update->dui = $DUI;
		$update->telefono = $telefono;
		$update->genero = $genero;
		$update->correo = $email;

		if($update->save()){
			return response('Datos actualizados', 200)->header('Content-Type', 'text/plain');	
		}else{
			return response('error en actualizar datos', 400)->header('Content-Type', 'text/plain');	
		}
	}

	public function updatePassword(){
		$password = $_POST["password"];
		$usuario = "";
		if(Auth::guard('participantes')->check()){
			$id = Auth::guard('participantes')->user()->id;
			$usuario = Participante::where('id' , '=' , $id)->first();
			if($password == null){
				
				return response('La contraseña se mantendra', 204)->header('Content-Type', 'text/plain');
			}else{
				$usuario->contrasena = password_hash($password,PASSWORD_DEFAULT);
				$this->checkIfPasswordChange($usuario);
			}
		}
		
		if(Auth::guard('tutor')->check()){
			$id = Auth::guard('tutor')->user()->tutor_id;
			$usuario = UsuarioTutor::where('tutor_id' , '=' , $id)->first();
			if($password == null){
				
				return response('La contraseña se mantendra', 204)->header('Content-Type', 'text/plain');
			}else{
				$usuario->password = password_hash($password,PASSWORD_DEFAULT);
				$this->checkIfPasswordChange($usuario);
			}		
		}
	}

	private function checkIfPasswordChange($usuario){
		if($usuario->save()){
			return response('Contraseña actualizada', 200)->header('Content-Type', 'text/plain');	
		}else{
			return response('Error actualizando la contraseña',400);
		}
	}

}