<?php

namespace App\Http\Controllers;
use App\Models\Diplomado;
use App\Models\Modulo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;



class DiplomadosController extends Controller
{
  public function index(){
    
    $Diplomados = Diplomado::get();
    $Especialidad = array();

    foreach($Diplomados as $Data){
      array_push($Especialidad,$Data->especialidad);
    }

    $data = [
      "Diplomados" => $Diplomados,
      "Especialidades" => array_unique($Especialidad)
    ];
    //$this->view('Diplomados/index', $data);
    return response()->json($data);
  }

  public function show($id){

    $Diplomado = Diplomado::find($id);
    $DiplomadoModulos = Modulo::where('iddiplomado',$id)->get(['id','nombre']);
    $data = [
      "Diplomado" => $Diplomado,
      "Modulos" => $DiplomadoModulos
    ];
    //$this->view("Diplomado/show",$data);
    return response()->json($data);
  }
}
?>
