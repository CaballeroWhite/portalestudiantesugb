<?php

namespace App\Http\Controllers;
use App\Models\ModuloUnidades;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ModuloController extends Controller {

    public function index(){

    }

    public function unidades($moduloId){
        $Unidades = ModuloUnidades::where('idModulo',$moduloId)->get([
            'id',
            'nombre'
        ]);
        
        if(count($Unidades) > 0){
            $data = [
                "count" => count($Unidades),
                "Unidades" => $Unidades
            ];
        }else{
            return response()->json([
                "message" => "No hay unidades para este Modulo"
            ]);
        }
        
        return response()->json($data);
    }
}