<?php
/**
 * 
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Seminario extends Model
{
	protected $fillable = ['nombre', 'descripcion', 'area_id', 'tutor_id', 'fechas', 'cupo', 'ubicacion'];
	function area(){
		return $this->belongsTo(Area::class);
	}
	function tutor(){
		return $this->belongsTo(Tutor::class);
	}
	function participantes(){
		return $this->belongsToMany(Participante::class);
	}
}
?>