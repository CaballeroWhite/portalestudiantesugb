<?php
  namespace App\Models;
  use Illuminate\Database\Eloquent\Model;

  /**
   *
   */
  class Modulo extends Model
  {
    public function diplomado(){
      return $this->belongsTo(Diplomado::class, 'iddiplomado');
    }
    public function grupos(){
      return $this->hasMany(DiplomadoGrupo::class);
    }
  }

?>
