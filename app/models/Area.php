<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model; // Importante en todos los modelos

/**
 *
 */
class Area extends Model
{
  protected $table = "areas";
  protected $fillable = ["nombre"];
  function tutores(){
  	return $this->hasMany(Tutor::class);
  }
}

?>
