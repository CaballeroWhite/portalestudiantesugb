<?php
  namespace App\Models;
  use Illuminate\Database\Eloquent\Model;

  /**
   *
   */
  class DiplomadoGrupo extends Model
  {
    protected $table = "diplomado_grupo";
    protected $fillable = ["cod", "modulo_id", "idperiodo","idtutor", "cupo",
      "fecha_inicio", "fecha_fin","ubicacion", "estado", "horario"];
    function participantes(){
      return $this->belongsToMany(Participantes::class)->withTimestamps();
    }
    function modulo(){
      return $this->belongsTo(Modulo::class);
    }
    function tutor(){
      return $this->belongsTo(Tutor::class, "idtutor");
    }

    // aqui le agregue esto by JOse luis
    public function diplomado(){
      return $this->belongsTo(Diplomado::class);
    }
  }

?>
