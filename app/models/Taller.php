<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Taller extends Model
{
	protected $table = "talleres" ;
	protected $fillable = ['nombre', 'area_id', 'tutor_id', 'fechas_taller', 'cantidad_h', 'descripcion', 'cupo', 'ubicacion', 'estado'];
	function area(){
		return $this->belongsTo(Area::class);
	}
	function tutor(){
		return $this->belongsTo(Tutor::class);
	}
	function participantes(){
		return $this->belongsToMany(Participante::class);
	}
}
?>