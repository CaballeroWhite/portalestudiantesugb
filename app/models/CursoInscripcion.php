<?php
/**
 * 
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class CursoInscripcion extends Model
{
	protected $table = "curso_grupo_participante";
	protected $fillable = ["curso_grupo_id","participante_id","estado"];
	function participante(){
    	return $this->belongsTo(Participante::class);
    }
}
?>