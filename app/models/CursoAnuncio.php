<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class CursoAnuncio extends Model
{
  protected $table = "curso_anuncios";
  protected $fillable = ['id',"Nombre", "Contenido", "Curso_id","grupoAnunciosId","nombreArchivo"];
  function curso(){
  	return $this->belongsTo(Curso::class);
  }

  function grupo(){
    return $this->belongsTo(GrupoAnuncio::class,'grupoAnunciosId');
  }
}

?>