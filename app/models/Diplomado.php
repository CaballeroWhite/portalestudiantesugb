<?php
  namespace App\Models;
  use Illuminate\Database\Eloquent\Model;

  class Diplomado extends Model
  {
    protected $table = "diplomados";
    protected $fillable = ["nombre","descripcion","especialidad","NumeroModulos"];
    public function modulos(){
      return $this->hasMany(Modulo::class, 'iddiplomado');
    }

    // aqui le agregue esto by JOse luis
    function diplomados(){
      return $this->hasMany(DiplomadoGrupo::class);
    }
  }

?>
