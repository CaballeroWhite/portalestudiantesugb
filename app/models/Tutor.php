<?php
namespace App\Models;

use App\UsuarioTutor;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class Tutor extends Model
{
  protected $table = "tutores";
  protected $fillable = ["nombres", "apellidos", "fecha_nacimiento", "area_id", "titulo", "dui", "telefono", "correo", "genero"];


  function cursoGrupos(){
    return $this->hasMany(CursoGrupo::class);
  }

  function usuario(){
    return $this->hasOne(UsuarioTutor::class);
  }



}

?>
