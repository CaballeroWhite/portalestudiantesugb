<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ExplorarModel extends Model{

    function getCursos(){

        return Curso::get();
    }

    function getDiplomados(){

        return Diplomado::get();
    }
    
}