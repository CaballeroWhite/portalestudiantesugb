<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class GrupoAnuncio extends Model{
    protected $table  = "grupoanuncioscurso";
    protected $fillable = ["id","nombreGrupo","Curso_id"];

    function getAnuncios(){
        return $this->hasMany(CursoAnuncio::class,"grupoAnunciosId");
    }


}
?>