<?php
/**
 * 
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class CursoGrupo extends Model
{
	protected $table = "curso_grupo";
	protected $fillable = ['cod', 'curso_id', 'periodo_id', 'tutor_id', 'fecha_inicio', 'fecha_finalizacion',
							'ubicacion', 'cupo'];
	public function curso(){
		return $this->belongsTo(Curso::class);
	}
	public function tutor(){
		return $this->belongsTo(Tutor::class);
	}
}
?>