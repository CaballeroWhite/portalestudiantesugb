<?php
namespace App\Models;
  use Illuminate\Database\Eloquent\Model;

  class Foro extends Model
  {
    protected $table = "foros";
    protected $fillable = ["Titulo","DescripcionForo","Fecha_Inicio_Foro","Cupos","Duracion_Estimada","Ubicacion","tutor_id"];
    
    function tutor(){
      return $this->belongsTo(Tutor::class);
    }
    
    function area(){
      return $this->belongsTo(Area::class);
    }
  
    function participantes(){
      return $this->belongsToMany(Participante::class);
    }
  }

?>