<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class CursoUnidad extends Model
{
  protected $table = "curso_unidades";
  protected $fillable = ["nombre", "idcurso"];
}

?>
