<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
/**
 *
 */
class Curso extends Model
{
  protected $table = "cursos";
  protected $fillable = ["nombre", "descripcion", "especialidad"];
  function grupos(){
  	return $this->hasMany(CursoGrupo::class);
  }

  function anuncios(){
    return $this->hasMany(CursoAnuncio::class);
  }
}

?>
