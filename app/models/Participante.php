<?php
  namespace App\Models;
  use Illuminate\Database\Eloquent\Model;

  /**
   *
   */
  class Participante extends Model
  {
    protected $fillable = [
      "nombres",
      "apellidos",
      "correo",
      "fecha_n",
      "genero",
      "nacionalidad",
      "ecivil",
      "dui",
      "nit",
      "telefono",
      "departamento",
      "municipio",
      "codigo",
      "contrasena"
    ];
    function gruposDiplomado(){
      return $this->belongsToMany(DiplomadoGrupo::class)->withTimestamps();
    }
    function gruposCurso(){
      return $this->belongsToMany(CursoGrupo::class)->withTimestamps();
    }
    function foros(){
      return $this->belongsToMany(Foro::class)->withTimestamps();
    }
    function talleres(){
      return $this->belongsToMany(Taller::class)->withTimestamps();
    }
    function seminarios(){
      return $this->belongsToMany(Seminario::class)->withTimestamps();
    }
    function getFullNameAttribute(){
      return $this->nombres.' '.$this->apellidos;
    }
    function getShortNameAttribute(){
      return explode(" ", $this->nombres)[0].' '.explode(" ", $this->apellidos)[0];
    }
  }

?>
