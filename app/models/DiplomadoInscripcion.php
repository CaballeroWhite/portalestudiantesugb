<?php
namespace App\Models;
  use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
  /**
   *
   */
  class DiplomadoInscripcion extends Pivot
  {
    protected $table = "diplomado_grupo_participante";
    protected $fillable = ["participante_id", "diplomado_grupo_id", "estado"];
    function participante(){
    	return $this->belongsTo(Participante::class);
    }
  }

?>
